﻿using System;
using System.IO;
using System.Text.RegularExpressions;

//This is my own code, Rikk Shimizu


namespace Exercise_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            
            //reused from programming project 3
            //I'll declare variables here that are neccesary for the txtBx returns.
            string textReadInFromFileInLongString;
            int numberOfTandE = 0;
            /*
             * reusing code from programming project 3 v2 to read in from a file using StreamReader.
             */
            using (StreamReader fileReadIn = new StreamReader("/CST-117_Individual_assignments/week6_exercise10/Exercise 10/Exercise 10/inputFile.txt"))
            {
                //here the .ReadToEnd goes through the entire file.
                textReadInFromFileInLongString = fileReadIn.ReadToEnd();
            }//ends using

            //using regex to find the count of t and e in the string.
            numberOfTandE = Regex.Matches(textReadInFromFileInLongString, "([te])(?![a-z])", RegexOptions.Multiline | RegexOptions.IgnoreCase).Count;
            Console.WriteLine("There are " + numberOfTandE + " words that in t or e");// print to the console, as shown in the instructions.


        }//ends main



    }//ends class
}//ends namespace
